package pl.sda.classes;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by adrian on 19.04.17.
 */
public class SomeClass {

    public SomeClass() {
        System.out.println("someClass constructor");
    }
}
