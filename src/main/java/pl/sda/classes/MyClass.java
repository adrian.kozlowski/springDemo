package pl.sda.classes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

/**
 * Created by adrian on 19.04.17.
 */
@Component
@SessionScope
public class MyClass {
    private Integer couter = 0;

    public MyClass() {
        System.out.println("MyClass constructor");
    }

    public Integer increase(){
        return ++this.couter;
    }
}
