package pl.sda.classes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * Created by adrian on 26.04.17.
 */
@Service
public class SimpleOrderManager {

    @Autowired
    private JavaMailSender mailSender;

    public void send() {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo("adrian.kozlowski@outlook.com");
        mail.setFrom("adrianspringtest@gmail.com");
        mail.setSubject("Zespoł JawaKtw - Rejestracja");
        mail.setText("Rejestracja użytkownika przebiegła pomyślnie, dziękujemy");
        mailSender.send(mail);
    }

}