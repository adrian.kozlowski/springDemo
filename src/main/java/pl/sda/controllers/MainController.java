package pl.sda.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.sda.classes.SimpleOrderManager;
import pl.sda.dto.PersonDto;
import pl.sda.exceptions.MyException;
import pl.sda.exceptions.SuperException;
import pl.sda.models.Person;
import pl.sda.repository.PersonRepository;
import pl.sda.services.CarService;

import javax.servlet.http.HttpServletRequest;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Principal;
import java.util.Collection;
import java.util.Random;

/**
 * Created by adrian on 20.04.17.
 */

@Controller
public class MainController {
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private CarService carService;

    @Autowired
    private SimpleOrderManager simpleOrderManager;

    private final Logger log = LoggerFactory.getLogger(MainController.class);


    @ExceptionHandler(SuperException.class)
    public ModelAndView handleSuperException(HttpServletRequest request, SuperException e) {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("exception", e.getMessage());
        modelAndView.setViewName("superException");
        return modelAndView;

    }


    @RequestMapping({"/", "/index"})
    public String main(ModelMap modelMap, Principal principal) {
        modelMap.addAttribute("rows",carService.getCarList());
        Authentication principal1 = (Authentication) principal;
        Collection<? extends GrantedAuthority> authorities = principal1.getAuthorities();
//                modelMap.addAttribute("isAdmin", false);
        for (GrantedAuthority authority : authorities) {

            if(authority.getAuthority().equals("ROLE_ADMIN")){
                modelMap.addAttribute("isAdmin", true);
            }
        }
        return "index";
    }

    @RequestMapping({"/send"})
    public String sendMail(ModelMap modelMap) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        simpleOrderManager.send();


        return "redirect:/";
    }

    @GetMapping("/hi")
    public ModelAndView index2(ModelMap modelMap, Principal principal) {
        return new ModelAndView("index2", modelMap);
    }

    @GetMapping("/login")
    public ModelAndView form(ModelMap modelMap) {
        log.info("asdasdasd");

//        modelMap.addAttribute("nameDto", new PersonDto());
        return new ModelAndView("form", modelMap);
    }


    @PostMapping("/hello")
    public ModelAndView saveForm(@ModelAttribute PersonDto nameDto, ModelMap modelMap) {
        modelMap.addAttribute("name", nameDto.getName());

        if (true) {
            throw new MyException("Wystąpił błąd na stronie");
        }

        return new ModelAndView("helloName", modelMap);
    }


    @ExceptionHandler(MyException.class)
    public void handleException() {
        log.error("coś poszło nie tak");
    }


    //spring.http.multipart.max-file-size=5MB

    @PostMapping("/")
    public String handleFileUpload(@RequestPart("file") MultipartFile file, RedirectAttributes redirectAttributes) {

        redirectAttributes.addFlashAttribute("message", "You successfully uploaded " + file.getOriginalFilename() + "!");
        try {
            byte[] bytes = file.getBytes();
            FileOutputStream fileOutputStream = new FileOutputStream("/home/adrian/IdeaProjects/demo/somefile.png");
            fileOutputStream.write(bytes);
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:/";
    }


    @RequestMapping("/index2")
    public ModelAndView secondPage(@RequestParam String color,
                                   @RequestParam String size,
                                   @RequestParam String price,
                                   ModelMap modelMap) {
        modelMap.addAttribute("color", color);
        modelMap.addAttribute("size", size);
        modelMap.addAttribute("price", price);
        return new ModelAndView("index2", modelMap);
    }
}
