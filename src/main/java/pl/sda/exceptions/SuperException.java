package pl.sda.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by adrian on 26.04.17.
 */
@ResponseStatus(value = HttpStatus.BAD_GATEWAY, reason = "Bo tak")
public class SuperException extends RuntimeException {
}
