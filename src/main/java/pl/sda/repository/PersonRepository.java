package pl.sda.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.models.Person;

/**
 * Created by adrian on 24.04.17.
 */
@Repository
public interface PersonRepository extends JpaRepository<Person,Integer> {
}
