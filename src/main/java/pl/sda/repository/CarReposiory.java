package pl.sda.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.models.Car;

/**
 * Created by adrian on 29.04.17.
 */
@Repository
public interface CarReposiory  extends JpaRepository<Car, Integer>{
}
