package pl.sda.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.models.User;

/**
 * Created by adrian on 29.04.17.
 */
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);
}
