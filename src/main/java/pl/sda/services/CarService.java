package pl.sda.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.models.Car;
import pl.sda.repository.CarReposiory;

import java.util.List;

/**
 * Created by adrian on 29.04.17.
 */
@Service
public class CarService {
    @Autowired
    private CarReposiory carReposiory;


    /**
     * Returns whole list of cars in database by requesting repository
     * @return List
     */
    public List<Car> getCarList() {
        return carReposiory.findAll();
    }
}
