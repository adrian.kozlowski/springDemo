package pl.sda.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.sda.dto.UserFormLoginDto;
import pl.sda.models.Role;
import pl.sda.models.User;
import pl.sda.repository.RoleRepository;
import pl.sda.repository.UserRepository;

import java.util.HashSet;

/**
 * Created by adrian on 29.04.17.
 */
@Service
public class UserService {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    public void save(UserFormLoginDto userDto){
        User user = new User();
        user.setUsername(userDto.getUsername());
        user.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        user.setRoles(new HashSet<>());
        Role userRole = roleRepository.findOne(1);
        user.getRoles().add(userRole);

        userRepository.save(user);

    }
}
