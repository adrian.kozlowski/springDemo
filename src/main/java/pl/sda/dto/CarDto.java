package pl.sda.dto;


import pl.sda.models.Person;

import javax.persistence.*;

public class CarDto {
    private Integer id;
    private Integer horsepow;
    private Integer momentum;
    private String mark;
    private String model;
    private Integer year;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHorsepow() {
        return horsepow;
    }

    public void setHorsepow(Integer horsepow) {
        this.horsepow = horsepow;
    }

    public Integer getMomentum() {
        return momentum;
    }

    public void setMomentum(Integer momentum) {
        this.momentum = momentum;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
}
