package pl.sda.dto;

import java.util.Date;

/**
 * Created by adrian on 26.04.17.
 */
public class MyError {
    private String message;
    private Date date;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
