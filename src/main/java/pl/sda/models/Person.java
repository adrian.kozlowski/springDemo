package pl.sda.models;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * Created by adrian on 24.04.17.
 */
@Entity
@Table(name = "persons")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String name;
    @Column
    private String surname;

    @ManyToMany
    @JoinTable(
            name = "person_address",
            joinColumns=@JoinColumn(name="person_id", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="address_id", referencedColumnName="id"))
    private List<Address> addressSet;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<Address> getAddressSet() {
        return addressSet;
    }

    public void setAddressSet(List<Address> addressSet) {
        this.addressSet = addressSet;
    }
}
