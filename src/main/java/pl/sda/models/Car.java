package pl.sda.models;


import javax.persistence.*;

/**
 * Created by adrian on 24.04.17.
 */
@Entity
@Table(name = "cars")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private Integer horsepow;
    @Column
    private Integer momentum;
    @Column
    private String mark;
    @Column
    private String model;
    @Column
    private Integer year;
    @ManyToOne
    @JoinColumn(name = "owner_id")
    private Person owner;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHorsepow() {
        return horsepow;
    }

    public void setHorsepow(Integer horsepow) {
        this.horsepow = horsepow;
    }

    public Integer getMomentum() {
        return momentum;
    }

    public void setMomentum(Integer momentum) {
        this.momentum = momentum;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }
}
